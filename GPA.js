// let A = false;
// let B = true;
// let C = false;
// let D = undefined;
// let E = undefined;
// let F = true;
// let G = 3;
// let H = 10;
// let I = -3;
// let J = 10;

// console.log(A && (B || !D)); //false
// console.log((B&&(!A || D)) != !F); //true
// console.log((C || (B && !F)) && (A || E)); //false
// console.log((A && (B === F)) || ((D || B) && !C)); //true
// console.log((C || ((F === !A) || (E === D))) === (!B || F)); //true
// console.log(!C && (J == H)); // true
// console.log((A || !B) || (H > I)); //true
// console.log(((B !== C) || (I > G)) !== (!A && B)); //false
// console.log((!A || (J === C)) && (E || (!C === !A))); //true
// console.log(((A || (H > I)) &&  (J >= H)) !== (B || A)); //false

let Neo;
let Indiana;
let Severusa;
let Aladin; 
let Neo_1 = 60.2;
let Neo_2 = 57.3;
let Neo_3 = 72.4;
let Neo_4 = 88;
let Indiana_1 = 78.2;
let Indiana_2 = 52.3;
let Indiana_3 = 66.4;
let Indiana_4 = 80;
let Severusa_1 = 75.2;
let Severusa_2 = 67.3;
let Severusa_3 = 54.4;
let Severusa_4 = 90;
let Aladin_1 = 80.2;
let Aladin_2 = 52.3;
let Aladin_3 = 68.4;
let Aladin_4 = 76;
let sum_1 = Neo_1 + Neo_2 + Neo_3 + Neo_4;
let sum_2 = Indiana_1 + Indiana_2 + Indiana_3 + Indiana_4;
let sum_3 = Severusa_1 + Severusa_2 + Severusa_3 + Severusa_4;
let sum_4 = Aladin_1 + Aladin_2 + Aladin_3 + Aladin_4;
let max_sum;
let max_sum_name;
let max_avg;
let GPA_1 = 0;
let GPA_2 = 0;
let GPA_3 = 0;
let GPA_4 = 0;

max_sum = sum_1;
max_sum_name = "Neo";
if(max_sum < sum_2){
    max_sum = sum_2;
    max_sum_name = "Indiana";
}
 if(max_sum < sum_3){
    max_sum = sum_3;
    max_sum_name = "Severusa";
 }
if(max_sum < sum_4){
    max_sum = sum_4;
    max_sum_name = "Aladin";
}


console.log("The best student according to sum of the scores - " + max_sum_name + ", The score - " + max_sum);
max_avg = max_sum / 4;
console.log("The best student according to average of the scores - " + max_sum_name + ", The average score - " + max_avg);
console.log("The percentages");
console.log("Noe:");
console.log("     " + "Subject 1: " + Neo_1 + "%");
console.log("     " + "Subject 2: " + Neo_2 + "%");
console.log("     " + "Subject 3: " + Neo_3 + "%");
console.log("     " + "Subject 4: " + Neo_4 + "%");
console.log("Indiana:");
console.log("     " + "Subject 1: " + Indiana_1 + "%");
console.log("     " + "Subject 2: " + Indiana_2 + "%");
console.log("     " + "Subject 3: " + Indiana_3 + "%");
console.log("     " + "Subject 4: " + Indiana_4 + "%");
console.log("Severus:");
console.log("     " + "Subject 1: " + Severusa_1 + "%");
console.log("     " + "Subject 2: " + Severusa_2 + "%");
console.log("     " + "Subject 3: " + Severusa_3 + "%");
console.log("     " + "Subject 4: " + Severusa_4 + "%");
console.log("Aladin:");
console.log("     " + "Subject 1: " + Aladin_1 + "%");
console.log("     " + "Subject 2: " + Aladin_2 + "%");
console.log("     " + "Subject 3: " + Aladin_3 + "%");
console.log("     " + "Subject 4: " + Aladin_4 + "%");

let GPA = 0;

GPA_1 = ((Neo_1 * 4) + (Neo_2 * 2) + (Neo_3 * 7) + (Neo_4 * 5)) / 18;
 

if((GPA_1 > 51) &&(GPA_1 < 60)){
  GPA = 0.5;
}
if((GPA_1 > 61) &&(GPA_1 < 70)){
      GPA = 1.0;
}
if((GPA_1 > 71) &&(GPA_1 < 80)){
   GPA = 2.0;
}
if((GPA_1 > 81) &&(GPA_1 < 90)){
   GPA = 3.0;
}
if((GPA_1 > 91) &&(GPA_1 < 100)){
   GPA = 4.0;
}

console.log("Noe:      " + GPA_1 + "   GPA: " + GPA);


/****************************/


GPA_2 = ((Indiana_1 * 4) + (Indiana_2 * 2) + (Indiana_3 * 7) + (Indiana_4 * 5)) / 18;


if((GPA_2 > 51) &&(GPA_2 < 60)){
  GPA = 0.5;
}
if((GPA_2 > 61) &&(GPA_2 < 70)){
      GPA = 1.0;
}
if((GPA_2 > 71) &&(GPA_2 < 80)){
   GPA = 2.0;
}
if((GPA_2 > 81) &&(GPA_2 < 90)){
   GPA = 3.0;
}
if((GPA_2 > 91) &&(GPA_2 < 100)){
   GPA = 4.0;
}

console.log("Indiana:  " + GPA_2 + "   GPA: " + GPA);

/****************************/

GPA_3 = ((Severusa_1 * 4) + (Severusa_2 * 2) + (Severusa_3 * 7) + (Severusa_4 * 5)) / 18;
 

if((GPA_3 > 51) &&(GPA_3 < 60)){
  GPA = 0.5;
}
if((GPA_3 > 61) &&(GPA_3 < 70)){
      GPA = 1.0;
}
if((GPA_3 > 71) &&(GPA_3 < 80)){
   GPA = 2.0;
}
if((GPA_3 > 81) &&(GPA_3 < 90)){
   GPA = 3.0;
}
if((GPA_3 > 91) &&(GPA_3 < 100)){
   GPA = 4.0;
}

console.log("Severusa: " + GPA_3 + "   GPA: " + GPA);

/*****************************/


GPA_4 = ((Aladin_1 * 4) + (Aladin_2 * 2) + (Aladin_3 * 7) + (Aladin_4 * 5)) / 18;
 

if((GPA_4 > 51) &&(GPA_4 < 60)){
  GPA = 0.5;
}
if((GPA_4 > 61) &&(GPA_4 < 70)){
      GPA = 1.0;
}
if((GPA_4 > 71) &&(GPA_4 < 80)){
   GPA = 2.0;
}
if((GPA_4 > 81) &&(GPA_4 < 90)){
   GPA = 3.0;
}
if((GPA_4 > 91) &&(GPA_4 < 100)){
   GPA = 4.0;
}

console.log("Aladin:   " + GPA_4 + "   GPA: " + GPA); 